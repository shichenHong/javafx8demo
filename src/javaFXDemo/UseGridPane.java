package javaFXDemo;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class UseGridPane extends Application{
	Stage window;
	
	public static void main(String[] args){
		launch(args);
	}


	@Override
	public void start(Stage primaryStage) throws Exception {
		window = primaryStage;
		window.setTitle("Learn to use GridPane Layout");
		
//		***********************************************************************
		GridPane grid = new GridPane();
		
//		Set the gap between elements and the border of layout
		grid.setPadding(new Insets(10, 10 , 10, 10));
		
//		Set the vertical gaps
		grid.setVgap(8);
		
//		Set the horizontal gaps
		grid.setHgap(10);
		
		Label nameLabel = new Label("User name");
		GridPane.setConstraints(nameLabel, 0, 0);
		
//		TextField with default text
		TextField nameInput= new TextField("Vincent hier");
		GridPane.setConstraints(nameInput, 1, 0);
		
		Label passLabel = new Label("Password: ");
		GridPane.setConstraints(passLabel, 0, 1);
		
//		TextField with promptText
		TextField passInput = new TextField();
		passInput.setPromptText("Enter Password");
		GridPane.setConstraints(passInput, 1, 1);
		
		Button loginButton = new Button("Log In");
		GridPane.setConstraints(loginButton, 1, 2);
		
		grid.getChildren().addAll(nameLabel, nameInput, passLabel, passInput, loginButton);
		
//		***********************************************************************

		Scene scene = new Scene(grid, 300, 200);
		
		window.setScene(scene);
		window.show();
	}

}
