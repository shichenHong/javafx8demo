package javaFXDemo;

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;


public class UseChoiceBox extends Application{
	Stage window;
	Scene scene;
	Button button;
	Label label;
	Label label2;
	public static void main(String[] args){
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		window = primaryStage;
		window.setTitle("Use droplist");
		button = new Button("Click here");
		
//		*******************************************************************************
		ChoiceBox<String> choiceBox = new ChoiceBox<>();
		
//		add items to choice box
		
//		add one item
		choiceBox.getItems().add("apple");
		choiceBox.getItems().add("banana");
		choiceBox.getItems().add("orange");
		
//		add severals items 
		choiceBox.getItems().addAll("bacon", "ham", "egg");
		
//		set default value
		choiceBox.setValue("bacon");
		
//		set actions
		label = new Label("");
		label2 = new Label("");
		
		choiceBox.setOnAction(e -> label.setText("You got" + " the " + getChoice(choiceBox)));
		
		choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> 
		label2.setText("Choice was " + oldValue + "; Now it is " + newValue));
		
		button.setOnAction(e -> label.setText("You chose the " + getChoice(choiceBox)));
		
		
//		*******************************************************************************
		int space = 50;
		VBox layout = new VBox(space);
		layout.setPadding(new Insets(space, space, space, space));
		layout.getChildren().addAll(choiceBox, button, label, label2);
		
		scene = new Scene(layout, 500, 300);
		window.setScene(scene);
		window.show();
	}
	
	private String getChoice(ChoiceBox<String> cbox){
		String food = cbox.getValue();
		return food;
	}
	
	
}
