package javaFXDemo;

import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.stage.*;


public class UseCombobox extends Application{

	Stage window;
	Scene scene;
	Button button;
	Label label1;
	ComboBox<String> comboBox;
	
	public static void main(String[] args){
		launch(args);
		
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		window = primaryStage;
		window.setTitle("Use ComboBox");
		
		label1 = new Label("");
//		************************************************************************
		comboBox = new ComboBox<>();
		comboBox.getItems().addAll("Vincent", "Stefan", "Christoph", "Konrad");
		comboBox.setPromptText("Please select a name");
		comboBox.setOnAction(e -> label1.setText(comboBox.getValue() + " is a cool guy."));
//		comboBox.setEditable(true);
		
		
//		************************************************************************
		button = new Button();
		button.setText("Click here");
		
		
		
		int space = 20;
		VBox vbox = new VBox(space);
		vbox.setPadding(new Insets(space, space, space, space));
		vbox.getChildren().addAll(button, comboBox, label1);
		
		scene = new Scene(vbox, 500, 300);
		window.setScene(scene);
		window.show();
	}
}
