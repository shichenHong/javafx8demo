package javaFXDemo;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

public class ValidateInput extends Application {

	Stage window = new Stage();
	Button button;
	Scene scene;
	
	
	public static void main(String[] args){
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		window = primaryStage;
		window.setTitle("Extract and Validate input");
		
		TextField nameInput = new TextField();
		button = new Button("Click here");
		button.setOnAction(e -> verifyInt(nameInput, nameInput.getText()));
		
		VBox layout = new VBox(10);
		layout.setPadding(new Insets(20, 20, 20, 20));
		layout.getChildren().addAll(nameInput, button);
		
		scene = new Scene(layout, 300, 250);
		window.setScene(scene);
		window.show();
		
	}
	
	public boolean verifyInt(TextField input, String message){
		try{
			int age = Integer.parseInt(input.getText());
			System.out.println("Age: " + age);
			return true;
			
		}catch(NumberFormatException e){
			System.out.println("Error: "+ message + " is not a number! ");
			
			return false;
		}
	}
	
	
	
	
}
