package javaFXDemo;

import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.control.*; //contains Button
import javafx.scene.layout.*;	// contains HBox, VBox, StackPane, etc.
import javafx.stage.*;

public class MultiLayout extends Application{
	
	Stage window;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public void start(Stage primaryStage) throws Exception{
		window = primaryStage;
		window.setTitle("MultiLayer GUI");
		
//		**********************************************************************
		HBox topMenu = new HBox();
		Button buttonA = new Button("File");
		Button buttonB = new Button("Edit");
		Button buttonC = new Button("View");
		topMenu.getChildren().addAll(buttonA, buttonB, buttonC);
		
		
		VBox leftMenu = new VBox();
		Button buttonD = new Button("D");
		Button buttonE = new Button("E");
		Button buttonF = new Button("F");
		leftMenu.getChildren().addAll(buttonD, buttonE, buttonF);
		
		BorderPane borderPane = new BorderPane();
		borderPane.setTop(topMenu);
		borderPane.setLeft(leftMenu);
		
//		**********************************************************************
		
//		StackPane layout = new StackPane();
		Scene scene = new Scene(borderPane, 300, 250);
		
		window.setScene(scene);
		window.show();
	} 
	
}