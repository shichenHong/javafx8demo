package javaFXDemo;

import javafx.application.Application;
import javafx.stage.*;
import javafx.event.*;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class AlertBox {
	static boolean result;
	public static boolean displayAlert(String title, String message){
		Stage window = new Stage();
		
//		initModality set the window to the front, probihits click auf the base window
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle(title);
		window.setMinWidth(250);
		
		Label label = new Label();
		label.setText(message);
		
		Button closeButton = new Button("Close the window");
		closeButton.setOnAction(e -> window.close());
		
		Button yesButton = new Button("Yes");
		yesButton.setOnAction(e -> {
			result = true;
			window.close();
					
		});

		Button noButton = new Button("No");
		noButton.setOnAction(e -> {
			result = false;
			window.close();
		});
		
		VBox alertLayout = new VBox(10);
		alertLayout.setAlignment(Pos.CENTER);
		alertLayout.getChildren().addAll(label, closeButton, yesButton, noButton);
		
		Scene scene = new Scene(alertLayout);
		window.setScene(scene);
		
//		hold the flow of thread
		window.showAndWait();
		return result;
	}

}
