package javaFXDemo;
import java.awt.Checkbox;

import javafx.application.*;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.geometry.*;
import javafx.stage.*;
import javafx.scene.control.*;


public class UseCheckBox extends Application{
	Stage window;
	Scene scene;
	Button button;
	
	public static void main(String[] args){
		launch(args);
	}
	
	@Override
	
	public void start(Stage primaryStage){
		window = primaryStage;
		window.setTitle("See the checkbox?");
//		************************************************************
//		CheckBox
		CheckBox box1 = new CheckBox("Bacon");
		CheckBox box2 = new CheckBox("Tuna");
		
//		checked by default
		box2.setSelected(true);
		
		
//		************************************************************
		String message = "";
		Label order = new Label(message);
		button = new Button("Click here");
		button.setOnAction(e -> order.setText(handleOption(box1, box2)));
		VBox layout = new VBox(20);
		layout.setPadding(new Insets(10,10,10,10));
		layout.getChildren().addAll(button, box1, box2, order);
		
		
		scene = new Scene(layout, 300, 280);
		window.setScene(scene);
		window.show();
	}

	private String handleOption(CheckBox box1, CheckBox box2) {
		String message = "You have ordered: \n";
		if(box1.isSelected())
			message += box1.getText() + "\n";
		
		if(box2.isSelected())
			message += box2.getText() + "\n";
		
		return message;
	}
}
