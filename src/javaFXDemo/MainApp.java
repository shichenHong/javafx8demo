package javaFXDemo;

import javafx.application.Application;
import javafx.stage.*;
import javafx.event.*;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class MainApp extends Application implements EventHandler<ActionEvent>{
	
//	declare the elements of GUI
	Stage window;
	Button button, button2, button3, button4, buttonClose;
	Scene scene1, scene2;
	
//	this main() method must always be written
	public static void main(String[] args){
		launch(args);
	}
	
//	start() is a method built in the Class Application
//	Override the start method, Stage as the argument
//		stage is like a container, in which all kinds of element to be put.
	@Override

	public void start(Stage primaryStage) throws Exception{
		
		window = primaryStage;
		
//		setOnCloseRequest appoint the code to be carried out before the window is closed
		window.setOnCloseRequest(e -> {
//			consume() method take over the closing process to block the default closing action
			e.consume();
			closeProgram();
		});
		
		Label welcome = new Label("Welcome to scene 1");
		
//		create an object of Button class
		button = new Button();
		button2 = new Button();
		button3 = new Button();
		button4 = new Button();
		buttonClose = new Button("Close properly");
		

//		set the text label of this button
		button.setText("Click here.");
		button2.setText("2nd Button");
		button3.setText("3rd Button");
		button4.setText("Go to Scene 2");
//		pass the event handler to the button. The handler is a method of the interface EventHandler, 
//		which defines the action by activating
		
//		3 variety to realize handler
		
//		1. write a handler method. the "this" here means, the handler is written within this class. The handler can also 
//			be written as a method within another class
		button.setOnAction(this);
		
//		2. use inner anonymous class
		button2.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				System.out.println("I used inner annonymous class!");
			}
		});
		
//		3. use lambda because the interface handler hat only one method 
		button3.setOnAction(e -> {
			System.out.println("lambda is the best!");
		});
		
		buttonClose.setOnAction(e -> closeProgram());
		
//		layout is an "organizer" of various elements for stage. Types of layout include: VBox, HBox, StackPane
		VBox layout = new VBox();
		
//		add the button to layout
		layout.getChildren().addAll(welcome, button, button2, button3, button4, buttonClose);
		
//		scene manages how the stage looks like, layout is its constructor argument, followed by width and height of GUI
//		a stage can have one or more scenes
		Scene scene = new Scene(layout, 300, 250);
		
//		************************************************************************************************
		Label welcome2 = new Label("Welcome to scene 2");

		Button button2_1 = new Button("Go back to scene 1");
		Button button2_2 = new Button("Alert");
		
		button2_1.setOnAction(event -> window.setScene(scene));
		button2_2.setOnAction(e -> {
		boolean answer = AlertBox.displayAlert("Alert Title", "I am an alert!");
		System.out.println(answer);
		});
		
		HBox layout2 = new HBox();
		layout2.getChildren().addAll(welcome2, button2_1, button2_2);
		layout2.setAlignment(Pos.TOP_CENTER);
		
		Scene scene2 = new Scene(layout2, 600, 500);
		
//		************************************************************************************************
//		button4 switch the stage from scene1 to scene2
		button4.setOnAction(event -> window.setScene(scene2));
		
//		pass the scene to stage
		window.setScene(scene);
		window.setTitle("Title of Window");   	
//		at the end let the stage display itself
		window.show();
	}

	@Override
	public void handle(ActionEvent event) {
		if(event.getSource() == button){
			System.out.println("I'm the first button ever created");
		}
	}
	
	public void closeProgram(){
		boolean answer = AlertBox.displayAlert("Confirm before quit", "Are you sure to exit the programm?");
		if(answer){
			System.out.println("That's how to close a GUI properly!");
			window.close();
		}else
			System.out.println("Program not close!");
	}
	
}
